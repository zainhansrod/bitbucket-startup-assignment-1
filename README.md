This repository contains a single index.html file. This is the main file for the project and all source code is contained within it. 
How to use:
1. Download the html file
2. double click on the file to open in your selected browser

License 
This product is free to use for both personal and commercial use. There are no restrictions on the usage of this code.

The reason I made this code free to use is because I would like everyone to have access to this and enhance it in many different ways. I am curious to see how people implement this code and make it better.

__________